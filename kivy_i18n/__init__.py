# -*- coding: utf-8 -*-
from kivy.event import EventDispatcher
from kivy.properties import DictProperty, StringProperty


class I18n(EventDispatcher):
    dict = DictProperty()
    lang = StringProperty('en')

    def load_language(self, lang, dictionary):
        self.dict = dictionary
        self.lang = lang

    def get(self, value):
        return self.dict[value]
