#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from distutils.core import setup

setup(name='kivy_i18n',
      version='0.1.0',
      description='A simple internationalization module for Kivy applications.',
      author='Andrés Rodríguez (mixedCase)',
      author_email='andres.rodriguez@lithersoft.com',
      url='https://gitlab.com/mixedCase/kivy-kivy_i18n',
      packages=['kivy_i18n'],
      requires=['kivy']
      )
